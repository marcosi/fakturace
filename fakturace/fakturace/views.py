# -*- coding: utf-8 -*- 

#from reportlab.pdfgen import canvas
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.contrib.auth import authenticate, login
from django.template import RequestContext
from models import katalog, osoba, faktury, polozky_fa

import datetime



def login(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            # Redirect to a success page.
            return redirect('/fa/')
        else:
            # Return a 'disabled account' error message
            return redirect('fa/login/')
    else:
        # Return an 'invalid login' error message.	
        return redirect('fa/login/')


def fakturaTisk(request,faktura_id='20130101'):
    '''
        vypis faktury dle zadaneho id na obrazovku
    '''
    if not request.user.is_authenticated():
        return render_to_response('error.html', {'title' : 'Access denied', 'message' : 'Pro pristup k teto strance musite byt prihlasen!!!'})
    try:
        FA = faktury.objects.get(fa_id = faktura_id)
        fa = {}
        fa['fa_id']     = faktura_id
        fa['vystaveno'] = FA.vystaveno.strftime('%d.%m. %Y')
        fa['splatnost'] = (FA.vystaveno + datetime.timedelta(days=FA.splatnost)).strftime('%d.%m. %Y')
        POL = FA.polozky_fa_set.all()
        fa['pocet_polozek'] = POL.count()
        fa['polozky']   = []
        fa['doprava']   = FA.doprava
        fa['platba']    = FA.platba
        fa['cena_celkem'] = 0
        for pol in POL.all():
            JED_CENA = int(float(pol.kat_id.cena)*float(FA.odberatel.sleva))
            DPH = (pol.kat_id.dph / float(100)) + 1
            CELKEM = round(int(DPH * JED_CENA * pol.count),-1)
            fa['polozky'].append({
                            'jmeno'         : pol.kat_id.jmeno ,
                            'dph'           : pol.kat_id.dph,
							'comment'		: pol.comment,
                            'cena_bez_slevy': round(int(DPH * pol.kat_id.cena), -1),
                            'pocet'         : pol.count,
                            'cena_dph'      : round(int(DPH * JED_CENA),-1),
                            'cena_zaklad'   : int(JED_CENA * pol.count),
                            'cena_celkem'   : CELKEM,
                            })
            fa['cena_celkem'] += CELKEM
                            
        fa['dodavatel'] = FA.dodavatel
        fa['odberatel'] = FA.odberatel
        fa['sleva'] = int(float(FA.odberatel.sleva)*100)
        return render_to_response("fakturaTisk.html", fa, context_instance=RequestContext(request))
    except KeyError:
        return render_to_response('fakturaChyba.html', {'hlaska':"pozadovana stranka nenalezena"})



def fakturaSeznam(request):
    """
    vypis seznamu faktur
    vraci:
        faktury.fa_id
        faktury.odberatel
        faktury.vystaveno
        faktury.celkem
        CELKEM
    """
    if not request.user.is_authenticated():
        return render_to_response('error.html', {'title' : 'Access denied', 'message' : 'Pro pristup k teto strance musite byt prihlasen!!!'})
    data = {'faktury' : []}
    CELKEM = 0
	# jeste poladit, pokud bude uzivatel admin, vypise se mu vse
	# faktury.objects.filter(odberatel__jmeno = request.user)
    for fa in faktury.objects.all():
        faktura = {}
        faktura['fa_id'] = fa.fa_id
        faktura['odberatel'] = fa.odberatel.jmeno
        faktura['vystaveno'] = fa.vystaveno
        faktura['celkem'] = 0
        for pol in fa.polozky_fa_set.all():
            JED_CENA = int(float(pol.kat_id.cena)*float(fa.odberatel.sleva))
            DPH = (pol.kat_id.dph / float(100)) + 1
            faktura['celkem'] += round(int(DPH * JED_CENA * pol.count),-1)
        data['faktury'].append(faktura)
        CELKEM += int(faktura['celkem']) 
    data['celkem'] = CELKEM
    return render_to_response("fakturaSeznam.html", data, context_instance=RequestContext(request))


def cenikTisk(request, detail_id = None):
	data  = {'cenik' : [], 'detail' : None}
	for pol in katalog.objects.all():
		polozka = {}
		for key, val in pol.__dict__.items():
			polozka[key] = val 
		polozka['cena_dph'] = int(round(polozka['cena'] * 1.21, -1))
		data['cenik'].append(polozka)
	try:
		DETAIL = katalog.objects.get(kat_id = detail_id)
	except:
		DETAIL = None
	finally:
		data['detail'] = DETAIL
	return render_to_response('cenikTisk.html', data)







#def pdfGen(request):
    #response = HttpResponse(content_type="aplication/pdf")
    #response['Content-Disposition'] = 'attachment; filename="somefilename.pdf"' 

    #p = canvas.Canvas(response)
    #p.drawString(100,100, "NEJAKY TEXT")
    #p.showPage()
    #p.save()
    #return response
