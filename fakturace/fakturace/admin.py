# -*- coding: utf-8 -*-

from django.contrib import admin
from models import osoba, katalog, faktury, polozky_fa, smlouvy, zakazky, objednavky

admin.site.register(osoba)
admin.site.register(katalog)
admin.site.register(smlouvy)
admin.site.register(zakazky)
admin.site.register(objednavky)

class polozkyInLine(admin.StackedInline):
    model = polozky_fa
    extra = 5

class fakturyAdmin(admin.ModelAdmin):
    list_display = ("fa_id","splatnost")
    fieldsets = [
        (None,      {'fields' : [
                        'fa_id',
                        'dodavatel',
                        'odberatel',
                        'doprava',
                        'platba',
                        'splatnost',
                        'vystaveno',
                        ]}),
        ]
    inlines = [ polozkyInLine ]

#admin.site.register(faktury)

admin.site.register(faktury, fakturyAdmin)
