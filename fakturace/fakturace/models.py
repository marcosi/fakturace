# -*- coding: utf-8 -*- 

from django.db import models
#from django import forms

#class myForm(forms.Form):
#    subject = forms.CharField(max_length=100)
#    message = forms.CharField()
#    sender = forms.EmailField()
#    cc_myself = forms.BooleanField(required=False)
    
    
class osoba(models.Model):
    choice_sleva = (
        ('1', "Bez slevy"),
        ('0.9', "Sleva 10%"),
        ('0.8', "Sleva 20%"),
        ('0.7', "Sleva 30%"),
        ('0.6', "Sleva 40%"),
        ('0.5', "Sleva 50%"),
        )
    os_id = models.AutoField(primary_key=True)
    jmeno = models.CharField(max_length=40, help_text="Vyplnte nazev spolecnosti, nebo Vase cele jmeno")
    adresa = models.TextField(help_text="Vlozte adresu ve tvaru: Jmeno\nUlice CP/CO\nPSC Mesto\n\nZEME")
    tel = models.CharField(blank=True, max_length=16, help_text="telefonni cislo v mezinarodnim formatu +420 XXX XXX XXX")
    email = models.EmailField()
    www = models.URLField(max_length=30)
    ico = models.CharField(max_length=8)
    dic = models.CharField(max_length=12,blank=True)
    sleva = models.CharField(max_length=4, choices=choice_sleva, default="Bez slevy")
    poc_serveru = models.IntegerField(blank=True,default=0)
    bank_jmeno = models.CharField("Jméno banky",blank=True, max_length=60, help_text="napriklad: BRE Bank S.A., organizační složka podniku (mBank)")
    bank_cu = models.CharField("Číslo účtu", blank=True, max_length=24)
    bank_bic = models.CharField("BIC", blank=True, max_length=8)
    bank_iban = models.CharField("IBAN", blank=True, max_length=30)
    bank_vs = models.IntegerField("VS", blank=True,max_length=10,default='0000000000')
    bank_ks = models.IntegerField("KS", blank=True,max_length=4,default='0008')
    bank_ss = models.IntegerField("SS", blank=True,max_length=10,default='0000000000')
    
    def __unicode__(self):
        return self.jmeno


class katalog(models.Model):
    kat_id = models.AutoField('katalogove cislo', primary_key=True)
    jmeno = models.CharField(max_length=100)
    popis = models.TextField(blank=True, help_text="popisek vyrobku")
    cena = models.IntegerField()
    dph = models.IntegerField()
    dostupnost = models.BooleanField()
    
    def __unicode__(self):
        return self.jmeno
        
#    def __getitem__(self):
#        return str(self.jmeno + '(' + self.cena + ')')


class smlouvy(models.Model):
    sml_id = models.BigIntegerField("cislo smlouvy", primary_key=True)
    dodavatel = models.ForeignKey('osoba',related_name='sml_dodavatel')
    odberatel = models.ForeignKey('osoba',related_name='sml_odberatel')
    platnost_do = models.DateField()
    cena = models.IntegerField()
    zadani = models.TextField(blank=True)

    def __getitem__(self):
        return self.sml_id
        

class objednavky(models.Model):
    obj_id = models.BigIntegerField(primary_key=True)
    dodavatel = models.ForeignKey('osoba',related_name='obj_dodavatel')
    odberatel = models.ForeignKey('osoba',related_name='obj_odberatel')
    dodani = models.DateField()
    cena = models.IntegerField()
    zadani = models.TextField(blank=True)
  
    def __getitem__(self):
        return self.obj_id
    
    
class zakazky(models.Model):
    zak_id = models.BigIntegerField(primary_key=True)
    dodavatel = models.ForeignKey('osoba',related_name='zak_dodavatel')
    odberatel = models.ForeignKey('osoba',related_name='zak_odberatel')
    dodani = models.DateField()
    cena = models.IntegerField()
    zadani = models.TextField(blank=True)

    def __getitem__(self):
        return self.zak_id
        
        
class faktury(models.Model):
    choice_platba = (
        ('prevodem','převodem'),
        ('hotove', 'hotové')
        )
    fa_id = models.BigIntegerField(primary_key=True)
    dodavatel = models.ForeignKey('osoba',related_name='fa_dodavatel')
    odberatel = models.ForeignKey('osoba',related_name='fa_odberatel')
    vystaveno = models.DateField()
    platba = models.CharField(max_length=15,choices=choice_platba)
    splatnost = models.IntegerField(default=14, help_text="pocet dni do splatnosti od vystaveni faktury")
    doprava = models.CharField(max_length=200,blank=True)
    #smlouva = models.ForeignKey('smlouvy',blank=True)
    #objednavka = models.ForeignKey('objednavky',blank=True)
    #zakazka = models.ForeignKey('zakazky',blank=True)
    misto_urceni = models.TextField(blank=True)
    
    def __getitem__(self):
        return self.fa_id


class polozky_fa(models.Model):
    fa_id = models.ForeignKey('faktury')
    kat_id = models.ForeignKey('katalog')
    count = models.IntegerField(default=1)
    comment	= models.CharField(max_length=200,blank=True)

    def __getitem__(self):
        return self.fa_id
